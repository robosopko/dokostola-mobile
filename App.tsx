/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the UI Kitten TypeScript template
 * https://github.com/akveo/react-native-ui-kitten
 *
 * Documentation: https://akveo.github.io/react-native-ui-kitten/docs
 *
 * @format
 */

import React from 'react';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {persistor, store} from './redux/store';
import {ApplicationProvider, IconRegistry,} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {light as theme, mapping,} from '@eva-design/eva';
import {AppNavigator} from './components/AppNavigator';


const App = (): React.ReactFragment => (
  <>
    <IconRegistry icons={EvaIconsPack}/>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ApplicationProvider mapping={mapping} theme={theme}>
          <AppNavigator/>
        </ApplicationProvider>
      </PersistGate>
    </Provider>
  </>
);

export default App;
