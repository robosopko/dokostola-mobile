import {MassesSearchResult} from '../typings/search';

export const getMassesFromApi = (townId: string, date: string): Promise<MassesSearchResult> => {
    return fetch(`https://api2.dokostola.sk/index.php/massesInTown?town=${townId}&date=${date}`)
        .then((response) => response.json())
        .then((response: MassesSearchResult) => {
            return response;
        })
        .catch((error) => {
            console.warn(error);
            return {};
        });
};
