import {Town, TownApiObject} from '../typings/api';
import {slugify} from '../utils/string';
import {LookupObject} from '../typings/common';
import {isDefined} from '../utils/defined';

export const getTownsFromApi = (): Promise<LookupObject<Town[]>> => {
    return fetch('https://api2.dokostola.sk/index.php/allTowns')
        .then((response) => response.json())
        .then((response: TownApiObject[]) => {
            const townsSearchMap: LookupObject<Town[]> = {};
            for (const townApiObject of response) {
                const slug = slugify(townApiObject.title);
                const town = {...townApiObject, slug};
                const searchIndex = slug.substr(0, 3);
                if (isDefined(townsSearchMap[searchIndex])) {
                    townsSearchMap[searchIndex].push(town);
                } else {
                    townsSearchMap[searchIndex] = [town];
                }
            }
            return townsSearchMap;
        })
        .catch((error) => {
            console.warn(error);
            return {};
        });
};
