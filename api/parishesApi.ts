import {Parishes} from '../typings/api';

export const getParishesFromApi = (): Promise<Parishes> => {
    return fetch('https://api2.dokostola.sk/index.php/allParishes')
        .then((response) => response.json())
        .then((response) => {
            return response;
        })
        .catch((error) => {
            console.warn(error);
            return {};
        });
};
