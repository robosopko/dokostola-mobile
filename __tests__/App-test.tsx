/**
 * @format
 */

import 'react-native';
import React from 'react';
import {shallow} from 'enzyme';
import App from '../App';

describe('App', () => {
  const wrapper = shallow(<App />);

  it('should render ApplicationProvider', () => {
    expect(wrapper.find('ApplicationProvider')).toHaveLength(1);
  });
});
