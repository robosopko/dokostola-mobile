import {Action} from 'typings/redux';
import {AppState} from '../typings/appState';
import {createSelector} from 'reselect';
import {isUndefined} from '../utils/defined';
import {ChurchWithMasses, MassesSearchResult} from '../typings/search';


export const FETCH_MASSES = 'FETCH_MASSES';
export const FETCH_MASSES_SUCCESS = 'FETCH_MASSES_SUCCESS';
export const FETCH_MASSES_ERROR = 'FETCH_MASSES_ERROR';
export const CLEAR_MASSES = 'CLEAR_MASSES';

export const fetchMassesAction = (): Action => ({type: FETCH_MASSES});
export const fetchMassesSuccessAction = (payload:MassesSearchResult): Action<MassesSearchResult> => ({type: FETCH_MASSES_SUCCESS, payload});
export const fetchMassesErrorAction = (payload: string): Action<string> => ({type: FETCH_MASSES_ERROR, payload});
export const clearMassesAction = (): Action => ({type: CLEAR_MASSES});

export const massesReducer = (state: MassesSearchResult = {}, action: Action<MassesSearchResult>) => {
    switch (action.type) {
        case FETCH_MASSES_SUCCESS: {
            return action.payload;
        }
        case CLEAR_MASSES: {
            return {};
        }
        default: {
            return state;
        }
    }
};

export const getMassesSearchResult = (state: AppState): MassesSearchResult => state.massesSearchResult;

export const getChurchWithMassesList = createSelector(
    [getMassesSearchResult],
    (massesSearchResult): ChurchWithMasses[] => {
        if (isUndefined(massesSearchResult.churches)) {
            return [];
        }

        return Object.values(massesSearchResult.churches!).map(church => {
            return {
                church,
                masses: isUndefined(massesSearchResult.masses) || massesSearchResult.masses!.length === 0 ? [] :
                    massesSearchResult.masses!.filter(mass => mass.churchId === church.id)
            };
        });
    }
);
