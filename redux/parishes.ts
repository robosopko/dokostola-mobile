import {Action} from 'typings/redux';
import {Parishes} from '../typings/api';
import {AppState} from '../typings/appState';


export const FETCH_PARISHES = 'FETCH_PARISHES';
export const FETCH_PARISHES_SUCCESS = 'FETCH_PARISHES_SUCCESS';

export const fetchParishesAction = (): Action => ({type: FETCH_PARISHES});
export const fetchParishesSuccessAction = (payload:Parishes): Action<Parishes> => ({type: FETCH_PARISHES_SUCCESS, payload});

export const parishesReducer = (state: Parishes = {}, action: Action<Parishes>) => {
    switch (action.type) {
        case FETCH_PARISHES_SUCCESS: {
            return action.payload;
        }
        default: {
            return state;
        }
    }
};

export const getParishes = (state: AppState): Parishes => state.parishes;
