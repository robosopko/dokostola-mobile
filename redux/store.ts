import AsyncStorage from '@react-native-community/async-storage';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import {persistReducer, persistStore} from 'redux-persist';
import {townsReducer} from './towns';
import {parishesReducer} from './parishes';
import createSagaMiddleware from 'redux-saga';
import {rootSaga} from '../sagas/rootSaga';
import { composeWithDevTools } from 'redux-devtools-extension';
import {searchReducer} from './search';
import {massesReducer} from './masses';

const rootReducer = combineReducers({
    towns: townsReducer,
    parishes: parishesReducer,
    search: searchReducer,
    massesSearchResult: massesReducer,
});

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: [
        'towns', 'parishes'
    ],
    blacklist: [
        'search', 'massesSearchResult'
    ],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
    persistedReducer,
    composeWithDevTools(
        applyMiddleware(
            sagaMiddleware,
            createLogger(),
        )
    ),
);

export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);
