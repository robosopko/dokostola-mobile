import {Town} from '../typings/api';
import {Action} from '../typings/redux';
import {AppState} from 'typings/appState';
import {LookupObject} from '../typings/common';
import {isDefined, isUndefined} from '../utils/defined';
import {createSelector} from 'reselect';
import {getTownSearchString} from './search';
import {slugify} from '../utils/string';


export const FETCH_TOWNS = 'FETCH_TOWNS';
export const FETCH_TOWNS_SUCCESS = 'FETCH_TOWNS_SUCCESS';
export const FETCH_TOWNS_ERROR = 'FETCH_TOWNS_ERROR';

export const fetchTownsAction = (): Action => ({type: FETCH_TOWNS});
export const fetchTownsSuccessAction = (payload: Town[]): Action<Town[]> => ({type: FETCH_TOWNS_SUCCESS, payload});
export const fetchTownsErrorAction = (payload: string): Action<string> => ({type: FETCH_TOWNS_ERROR, payload});

export const townsReducer = (state: LookupObject<Town[]> = {}, action: Action<LookupObject<Town[]>>) => {
    switch (action.type) {
        case FETCH_TOWNS_SUCCESS: {
            return action.payload;
        }
        default: {
            return state;
        }
    }
};

export const getAllTowns = (state: AppState): LookupObject<Town[]> => (state.towns);

export const getSearchedTowns = createSelector(
    [getAllTowns, getTownSearchString],
    (towns, searchString): Town[] => {
        if (isUndefined(searchString) || searchString.length < 3) {
            return [];
        }
        const searchSlug = slugify(searchString);
        const lookupString = searchSlug.substr(0, 3);
        const mappedTowns = isDefined(towns[lookupString]) ? towns[lookupString] : [];

        if (searchSlug.length === 3) {
            return mappedTowns;
        }

        return mappedTowns.filter(town => town.slug.startsWith(searchSlug));
    }
);
