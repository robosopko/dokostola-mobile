import {Action} from '../typings/redux';
import {AppState, SearchParams} from 'typings/appState';
import {Town} from '../typings/api';
import {getDateString} from '../utils/string';

export const SET_TOWN_SEARCH_STRING = 'SET_TOWN_SEARCH_STRING';
export const SET_SELECTED_TOWN = 'SET_SELECTED_TOWN';
export const SET_SELECTED_DATE = 'SET_SELECTED_DATE';

export const setTownSearchStringAction = (payload: string): Action<string> => ({type: SET_TOWN_SEARCH_STRING, payload});
export const setSelectedTownAction = (payload: Town): Action<Town> => ({type: SET_SELECTED_TOWN, payload});
export const setSelectedDateAction = (payload: string): Action<string> => ({type: SET_SELECTED_DATE, payload});

const defaultState: SearchParams = {
    townSearchString: '',
    selectedDate: getDateString(new Date())
};

export const searchReducer = (state: SearchParams = defaultState, action: Action<string | Town>) => {
    switch (action.type) {
        case SET_TOWN_SEARCH_STRING: {
            return {
                ...state,
                townSearchString: action.payload
            };
        }
        case SET_SELECTED_TOWN: {
            return {
                ...state,
                selectedTown: action.payload
            };
        }
        case SET_SELECTED_DATE: {
            return {
                ...state,
                selectedDate: action.payload
            };
        }
        default: {
            return state;
        }
    }
};

export const getTownSearchString = (state: AppState): string => (state.search.townSearchString);
export const getSelectedTown = (state: AppState): Town | undefined => (state.search.selectedTown);
export const getSelectedDate = (state: AppState): string => (state.search.selectedDate);
