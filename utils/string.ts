
export const slugify = (str: string) => {
    const map = new Map( [
        ['a', 'á|ä'],
        ['c', 'č'],
        ['d', 'ď'],
        ['e', 'é|ě'],
        ['i', 'í'],
        ['l', 'ĺ|ľ'],
        ['n', 'ň'],
        ['o', 'ó|ô'],
        ['r', 'ŕ|ř'],
        ['s', 'š'],
        ['t', 'ť'],
        ['u', 'ú|ü'],
        ['y', 'ý'],
        ['z', 'ž']
    ]);

    str = str.toLowerCase();

    for (const [key, value] of map) {
        str = str.replace(new RegExp(value, 'g'), key);
    }

    return str;
};

export const getDateString = (date: Date): string => `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
