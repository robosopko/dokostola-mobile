export const isDefined = (x: any): boolean => (x !== undefined && x !== null);
export const isUndefined = (x: any): boolean => (x === undefined || x === null);

