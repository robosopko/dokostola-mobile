/**
 * Standardized form of an Action for the application.
 */
export interface Action<T = {}> {
    type: string;
    payload?: T;
}
