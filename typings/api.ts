import {LookupObject} from './common';

export interface TownApiObject {
    id: string;
    title: string;
    alias: string;
    countyId: string;
}

export interface Town extends TownApiObject {
    slug: string;
}

export type Towns = LookupObject<Town>

export interface Parish {
    id: string;
    title: string;
    alias: string;
    deaneryId: string;
}

export type Parishes = LookupObject<Parish>

export interface ImageApiObject {
    parentId: string;
    fullImage: string;
    thumbImage: string;
    title: string;
    basePath: string;
}

export interface Church {
    id: string;
    title: string;
    alias: string;
    typeId: string;
    parishId: string;
    townId: string;
    images: ImageApiObject[];
    seasonMassesFrom: string;
    seasonMassesTo: string;
}

export interface Mass {
    id: string;
    churchId: string;
    day?: string;
    date?: string;
    time: string;
    languageId: string;
    focusId: string;
    note: string;
    isExtraordinary: boolean;
    overrideRegular: boolean;
    noMassToday: boolean;
    isSeasonal: boolean;
}
