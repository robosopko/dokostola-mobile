/**
 * An object that stores entries of a specific type of object
 * for easy lookup within the redux state.
 */
export interface LookupObject<T> {
    [identifier: string]: T;
}
