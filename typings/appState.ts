import {Parishes, Town} from './api';
import {LookupObject} from './common';
import {MassesSearchResult} from './search';

export interface SearchParams {
    readonly townSearchString: string;
    readonly selectedDate: string;
    readonly selectedTown?: Town;
}

export interface AppState {
    readonly towns: LookupObject<Town[]>;
    readonly parishes: Parishes;
    readonly search: SearchParams;
    readonly massesSearchResult: MassesSearchResult;
}
