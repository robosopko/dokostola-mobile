import {LookupObject} from './common';
import {Church, Mass} from './api';

export interface MassesSearchResult {
    churches?: LookupObject<Church>;
    masses?: Mass[];
}

export interface ChurchWithMasses {
    church: Church;
    masses: Mass[];
}

