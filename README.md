# DoKostola Mobile (react-native)

## Requirements

* Node.js 10  (latest LTS version)
* NPM 6.13 (latest LTS version)
* npx 6.13 (latest LTS version)

## Setup

    npm install
    cd ios && pod install
    
## Test

    npm run lint
    npm run test
    
## Run in iOS Simulator

    npx react-native run-ios
    
## Run in Android Simulator

    npx react-native run-android
