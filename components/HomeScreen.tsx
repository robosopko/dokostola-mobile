import React from 'react';
import {NavigationScreenProp, NavigationState, SafeAreaView} from 'react-navigation';
import {Button, Layout, Text, ThemedComponentProps, ThemeType, withStyles} from '@ui-kitten/components';
import I18n from './locales/config';
import {TopNavigationComponent} from './common/TopNavigation';

interface HomeScreenProps extends ThemedComponentProps {
    navigation: NavigationScreenProp<NavigationState>;
}

export const HomeScreen: React.FC<HomeScreenProps> = (props) => {

    const {themedStyle, navigation} = props;

    const navigateToSearch = () => {
        navigation.navigate('Search');
    };

    return (
        <SafeAreaView style={themedStyle.container}>
            <TopNavigationComponent title='DoKostola.sk' navigation={navigation}/>
            <Layout style={themedStyle.mainLayout}>
                <Text category='h1'>{I18n.t('hello')}</Text>
                <Button onPress={navigateToSearch}>OPEN SEARCH</Button>
            </Layout>
        </SafeAreaView>
    );
};

export const ThemedHomeScreen = withStyles(HomeScreen, (theme:ThemeType) => ({
    container: {
        flex: 1,
        backgroundColor: theme['color-danger-600'],
    },
    mainLayout: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
}));
