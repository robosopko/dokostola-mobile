import React from 'react';
import {Divider, Icon, TopNavigation, TopNavigationAction} from '@ui-kitten/components';
import {NavigationScreenProp, NavigationState} from 'react-navigation';

const BackIcon = () => (
    <Icon name='arrow-back' />
);

interface TopNavigationProps {
    title: string;
    navigation: NavigationScreenProp<NavigationState>;
}

export const TopNavigationComponent: React.FC<TopNavigationProps> = (props) => {
    const {title, navigation} = props;

    const navigateBack = () => {
        navigation.goBack();
    };

    const BackAction = () => (
        navigation.isFirstRouteInParent() ? undefined : <TopNavigationAction icon={BackIcon} onPress={navigateBack}/>
    );

    return (
        <>
            <TopNavigation title={title} alignment='center' leftControl={BackAction()}/>
            <Divider/>
        </>
    );
};
