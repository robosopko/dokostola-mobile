import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {ThemedHomeScreen} from './HomeScreen';
import {SearchScreen} from './search/SearchScreen';
import {MassesListScreen} from './search/MassesListScreen';

const HomeNavigator = createStackNavigator({
    Home: ThemedHomeScreen,
    Search: SearchScreen,
    Masses: MassesListScreen,
}, {
    headerMode: 'none',
});

export const AppNavigator = createAppContainer(HomeNavigator);
