import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {NavigationScreenProp, NavigationState, SafeAreaView} from 'react-navigation';
import {
    Button,
    Icon,
    Input,
    Layout,
    List,
    ListItem,
    Modal,
    Text,
    ThemedComponentProps,
    withStyles
} from '@ui-kitten/components';
import {fetchTownsAction, getAllTowns, getSearchedTowns} from '../../redux/towns';
import {AppState} from '../../typings/appState';
import {Town} from '../../typings/api';
import {ListRenderItemInfo} from 'react-native';
import {TopNavigationComponent} from '../common/TopNavigation';
import I18n from '../locales/config';
import {
    getSelectedDate,
    getSelectedTown,
    getTownSearchString,
    setSelectedDateAction,
    setSelectedTownAction,
    setTownSearchStringAction
} from '../../redux/search';
import {LookupObject} from '../../typings/common';
import {isDefined, isUndefined} from '../../utils/defined';
import {clearMassesAction} from '../../redux/masses';
import {Calendar, DateObject} from 'react-native-calendars';

interface SearchScreenProps extends ThemedComponentProps {
    navigation: NavigationScreenProp<NavigationState>;
}

export const SearchScreenPresenter: React.FC<SearchScreenProps> = (props) => {

    const { themedStyle, navigation} = props;

    const [calendarVisible, setCalendarVisible] = useState(false);
    const townSearchString: string = useSelector((state: AppState) => getTownSearchString(state));
    const allTowns: LookupObject<Town[]> = useSelector((state: AppState) => getAllTowns(state));
    const searchedTowns: Town[] = useSelector((state: AppState) => getSearchedTowns(state));
    const selectedTown: Town | undefined = useSelector((state: AppState) => getSelectedTown(state));
    const selectedDate: string = useSelector((state: AppState) => getSelectedDate(state));
    const dispatch = useDispatch();
    const setTownSearchString = (text: string) => dispatch(setTownSearchStringAction(text));
    const fetchTowns = () => dispatch(fetchTownsAction());
    const setSelectedTown = (town: Town) => dispatch(setSelectedTownAction(town));
    const setSelectedDate = (date: DateObject) => {
        dispatch(setSelectedDateAction(date.dateString));
        setCalendarVisible(!calendarVisible);
    };
    const clearMasses = () => dispatch(clearMassesAction());

    useEffect(() => {
        if (Object.keys(allTowns).length === 0) {
            fetchTowns();
        }
    });

    const renderSearchIcon = () => (
        <Icon  name='search-outline'/>
    );

    const renderCalendarIcon = () => (
        <Icon name='calendar'/>
    );

    const renderListItem = ({item}: ListRenderItemInfo<Town>) => (
        <ListItem
            title={item.title}
            onPress={() => setSelectedTown(item)}
        />
    );

    const navigateToMasses = () => {
        clearMasses();
        navigation.navigate('Masses');
    };

    return (
        <SafeAreaView style={themedStyle.container}>
            <TopNavigationComponent title={I18n.t('search')} navigation={navigation}/>
            <Layout style={themedStyle.mainLayout}>
                <Text category='h1' style={themedStyle.basicText}>
                    {I18n.t('search')}
                </Text>
                <Text category='s1' style={themedStyle.basicText}>
                    {`${I18n.t('selectedDate')}:`}
                </Text>
                <Button
                    onPress={() => {setCalendarVisible(!calendarVisible)}}
                    icon={renderCalendarIcon}
                    status='danger'
                    style={themedStyle.basicInput}
                >
                    {selectedDate}
                </Button>
                <Modal
                    visible={calendarVisible}
                    backdropStyle={themedStyle.backdrop}
                    allowBackdrop={true}
                    onBackdropPress={() => console.log('modal backdrop press')}
                >
                    <Calendar
                        style={themedStyle.calendar}
                        firstDay={1}
                        markedDates={{
                            [selectedDate]: {selected: true}
                        }}
                        onDayPress={setSelectedDate}
                    />
                </Modal>
                <Text category='s1' style={themedStyle.basicText}>
                    {`${I18n.t('selectedTown')}: ${isDefined(selectedTown) ? selectedTown!.title : ''}`}
                </Text>
                <Input
                    value={townSearchString}
                    icon={renderSearchIcon}
                    onChangeText={setTownSearchString}
                    style={themedStyle.basicInput}
                />
                <List
                    data={searchedTowns}
                    renderItem={renderListItem}
                    style={themedStyle.basicInput}
                />
                <Button
                    onPress={navigateToMasses}
                    icon={renderSearchIcon}
                    status='danger'
                    disabled={isUndefined(selectedTown)}
                    style={themedStyle.basicInput}
                >
                    {I18n.t('searchMasses')}
                </Button>
            </Layout>
        </SafeAreaView>
    );
};

export const SearchScreen = withStyles(SearchScreenPresenter, (theme) => ({
    container: {
        flex: 1,
        backgroundColor: theme['color-danger-600'],
    },
    mainLayout: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    basicText: {
        marginVertical: 5,
    },
    basicInput: {
        marginVertical: 5,
    },
    calendar: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        width: 300
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
}));
