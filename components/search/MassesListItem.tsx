import React from 'react';
import {Avatar, Icon, Text, ThemedComponentProps, withStyles} from '@ui-kitten/components';
import {ImageApiObject, Mass} from '../../typings/api';
import {TouchableOpacity, View} from 'react-native';
import {isDefined} from '../../utils/defined';
import {ChurchWithMasses} from '../../typings/search';
import I18n from '../locales/config';

interface MassesListItemProps extends ThemedComponentProps {
    item: ChurchWithMasses;
}

const getAvatarUrl = (images: ImageApiObject[]): string | undefined => {
    if (images.length === 0) {
        return undefined;
    }
    return `${images[0].basePath}/${images[0].thumbImage}`;
};

const getMassNote = (mass: Mass, allMasses: Mass[]): string => {
    const noteParts: string[] = [];
    const someMassHasDifferentLanguage = allMasses.some(m => m.languageId !== '0');
    if (mass.languageId !== '0' || someMassHasDifferentLanguage) {
        noteParts.push(I18n.t(`language_${mass.languageId}`));
    }
    if (mass.focusId !== '0') {
        noteParts.push(I18n.t(`focus_${mass.focusId}`));
    }
    if (mass.note.length > 0) {
        noteParts.push(mass.note);
    }
    return noteParts.join(', ');
};

const getMassesTimes = (masses: Mass[]): string | undefined => {
    if (masses.length === 0) {
        return undefined;
    }
    return masses.reduce((massesInfo: string, mass: Mass) => {
        const note = getMassNote(mass, masses);
        if (note.length > 0) {
            massesInfo += `${mass.time.substr(0,5)} (${note}); `
        } else {
            massesInfo += `${mass.time.substr(0,5)}; `
        }
        return massesInfo;
    }, '').slice(0, -2);
};

export const MassesListItemPresenter: React.FC<MassesListItemProps> = (props) => {

    const { item, themedStyle} = props;

    const avatarUrl = getAvatarUrl(item.church.images);

    return (
        <TouchableOpacity
            activeOpacity={0.7}
            style={themedStyle.container}
            onPress={() => console.log('MassesListItem onPress')}>
            <View style={themedStyle.leftSection}>
                {isDefined(avatarUrl)
                    ? <Avatar source={{uri: avatarUrl}}/>
                    : <Icon name='home-outline'/>
                }
                <View style={themedStyle.infoContainer}>
                    <Text
                        category='s2'>
                        {`${item.church.title}`}
                    </Text>
                    <Text
                        appearance='hint'
                        category='c2'
                    >
                        {getMassesTimes(item.masses)}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export const MassesListItem = withStyles(MassesListItemPresenter, (theme) => ({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 5,
        paddingVertical: 10,
    },
    infoContainer: {
        flex: 1,
        paddingLeft: 10,
    },
    leftSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
}));
