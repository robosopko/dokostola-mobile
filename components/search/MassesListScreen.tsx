import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {NavigationScreenProp, NavigationState, SafeAreaView} from 'react-navigation';
import {Layout, List, Text, ThemedComponentProps, withStyles} from '@ui-kitten/components';
import {AppState} from '../../typings/appState';
import {Town} from '../../typings/api';
import {ListRenderItemInfo} from 'react-native';
import {TopNavigationComponent} from '../common/TopNavigation';
import I18n from '../locales/config';
import {getSelectedDate, getSelectedTown} from '../../redux/search';
import {isDefined} from '../../utils/defined';
import {fetchMassesAction, getChurchWithMassesList} from '../../redux/masses';
import {MassesListItem} from './MassesListItem';
import {ChurchWithMasses} from '../../typings/search';

interface MassesListScreenProps extends ThemedComponentProps {
    navigation: NavigationScreenProp<NavigationState>;
}

export const MassesListScreenPresenter: React.FC<MassesListScreenProps> = (props) => {

    const { themedStyle, navigation} = props;

    const selectedTown: Town | undefined = useSelector((state: AppState) => getSelectedTown(state));
    const selectedDate: Date = useSelector((state: AppState) => getSelectedDate(state));
    const churchWithMassesList: ChurchWithMasses[] = useSelector((state: AppState) => getChurchWithMassesList(state));
    const dispatch = useDispatch();
    const fetchMasses = () => dispatch(fetchMassesAction());

    useEffect(() => {
        if (churchWithMassesList.length === 0) {
            fetchMasses();
        }
    });

    const renderListItem = ({item}: ListRenderItemInfo<ChurchWithMasses>) => {
        return (
            <MassesListItem
                item={item}
            />
        );
    };

    return (
        <SafeAreaView style={themedStyle.container} forceInset={{ vertical: 'always'}}>
            <TopNavigationComponent title={I18n.t('masses')} navigation={navigation}/>
            <Layout style={themedStyle.mainLayout}>
                <Text category='h1' style={themedStyle.basicText}>
                    {I18n.t('masses')}
                </Text>
                <Text category='s1' style={themedStyle.basicText}>
                    {`${I18n.t('selectedDate')}: ${selectedDate}`}
                </Text>
                <Text category='s1' style={themedStyle.basicText}>
                    {`${I18n.t('selectedTown')}: ${isDefined(selectedTown) ? selectedTown!.title : ''}`}
                </Text>
                <List
                    data={churchWithMassesList}
                    renderItem={renderListItem}
                    style={themedStyle.basicText}
                />
            </Layout>
        </SafeAreaView>
    );
};

export const MassesListScreen = withStyles(MassesListScreenPresenter, (theme) => ({
    container: {
        flex: 1,
        backgroundColor: theme['color-danger-600'],
    },
    mainLayout: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    basicText: {
        marginVertical: 5,
    },
}));
