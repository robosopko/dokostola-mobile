import { takeLatest, put, call } from 'redux-saga/effects';
import {FETCH_TOWNS, fetchTownsSuccessAction} from '../redux/towns';
import {getTownsFromApi} from '../api/townsApi';

function* doFetchTowns() {
    try {
        const towns = yield call(getTownsFromApi);
        yield put(fetchTownsSuccessAction(towns));
    }
    catch (error) {
        console.log(error);
    }
}

export const watchFetchTowns = takeLatest(FETCH_TOWNS, doFetchTowns);
