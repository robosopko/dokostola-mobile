import {all} from 'redux-saga/effects';
import {watchFetchTowns} from './townSaga';
import {watchFetchParishes} from './parishesSaga';
import {watchFetchMasses} from './massesSaga';

export function* rootSaga () {
    yield all([
        watchFetchTowns,
        watchFetchParishes,
        watchFetchMasses,
    ]);
}
