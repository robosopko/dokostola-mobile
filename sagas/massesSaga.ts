import {call, put, select, takeLatest} from 'redux-saga/effects';
import {FETCH_MASSES, fetchMassesSuccessAction} from '../redux/masses';
import {getMassesFromApi} from '../api/massesApi';
import {getSelectedDate, getSelectedTown} from '../redux/search';
import {Town} from '../typings/api';

function* doFetchMasses() {
    try {
        const town: Town = yield select(getSelectedTown);
        const date: string = yield select(getSelectedDate);

        const masses = yield call(getMassesFromApi, town.id, date);
        yield put(fetchMassesSuccessAction(masses));
    }
    catch (error) {
        console.log(error);
    }
}

export const watchFetchMasses = takeLatest(FETCH_MASSES, doFetchMasses);
