import { takeLatest, put, call } from 'redux-saga/effects';
import {getParishesFromApi} from '../api/parishesApi';
import {FETCH_PARISHES, fetchParishesSuccessAction} from '../redux/parishes';

function* doFetchParishes() {
    try {
        const parishes = yield call(getParishesFromApi);
        yield put(fetchParishesSuccessAction(parishes));
    }
    catch (error) {
        console.log(error);
    }
};

export const watchFetchParishes = takeLatest(FETCH_PARISHES, doFetchParishes);
